try:
	from .config import ELK_CONFIG
except:
	DEFAULT_ELK_CONFIG = """
### Configuration of Elasticsearch link ###
ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]
"""
	with open("config/config.py", "a") as config_file:
		config_file.write(DEFAULT_ELK_CONFIG)

	try:
		from .config import ELK_CONFIG
	except:
		ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]

try:
	from .config import GLOBAL_CONFIG
except:
	DEFAULT_GLOBAL_CONFIG = """
### Global config to the EvtxToElk class ###
GLOBAL_CONFIG = {
		"copy_field": {},
		"force_type": {
			"Id": "str"
		},
		"let_empty_properties": False,
		"let_empty_values": False,
		"let_rawdata": False,
		"let_rawevent": False,
		"add_message_event": True
	}
"""
	with open("config/config.py", "a") as config_file:
		config_file.write(DEFAULT_GLOBAL_CONFIG)

	try:
		from .config import GLOBAL_CONFIG
	except:
		GLOBAL_CONFIG = None