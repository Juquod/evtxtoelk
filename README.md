[![By cybex-assistance](./img/logo-cybex.png)](https://cybex-assistance.com/)



# EvtxToElk

EvtxToElk is a package to parse Evtx logs in the goal of sending them into an Elasticsearch database.

###### Resume

Into Kibana, the final result looks like a precise split of datas, even EventData. It's usefull to highlight every important details at a glance.

![Visualization from Kibana](./img/kibana_events1.png)

With every field possible into each events :

![Visualization from Kibana](./img/kibana_events2.png)

This simplifies a lot researches into logs and how to find just fields values.

###### Graphs

In fact, graphs are added, linked directly from the results of EvtxToElk tools (See graphs [here](visualizations_kibana)).

Example of a [graph of sessions](visualizations_kibana/gantt_sessions.json) :

![Visualization from Kibana](./img/session_graph1.png)

### Install

To install every dependencies, you need to execute :

```sh
sudo pip3 install -e . # Or sudo python3 -m pip install -e .
```

OR

```sh
sudo python3 setup.py install
```

If there is some problems with packages, launch `install_ubuntu_debian_dependencies.sh` to correct problems (In [installer](./installer) directory).

This library has been coded for Debian and works also on Ubuntu. Traduce the shell installer if you want to run it elsewhere.

### Launch

To launch the treatment of the log files, you can use the launch script :

```sh
evtxtoelk_launcher -h
```

It's easy to insert logs into the database with :

```sh
evtxtoelk_launcher -i my_index -f my_file.evtx
```

or even a complete directory (recursive search of evtx files) :

```sh
evtxtoelk_launcher -i my_index -d my_directory
```

### Configuration

In the launcher `config` directory, we can find the `config.py` file. (If he does not exist, run the `evtxtoelk_launcher` one time and it will appears).

#### Database configuration

To configure the link to you're database, you can open the config file and modify the variable `ELK_CONFIG`. (You can learn how to configure it in the [Elasticsearch official site](https://elasticsearch-py.readthedocs.io))

Example :

```python
ELK_CONFIG = [{ 'host': 'localhost', 'port': 9200 }]
```

#### GLOBAL_CONFIG

Into this config file, we can also find the variable `GLOBAL_CONFIG` and it's like that :

```python
GLOBAL_CONFIG = {
    "add_message_event": False,
    "copy_field": {},
    "force_type": {
        "Data.RawData": "str",
        "Data.RawMessage": "str"
    },
    "let_empty_properties": False,
    "let_empty_values": False,
    "let_rawdata": False,
    "let_rawevent": False,
    "modif_moins_to_None": True,
    "rawdata_traitment_fields": [
        "Data."
    ]
}
```

If we decompose it, we find some field useful depending of treated logs.

###### add_message_event

The boolean field `add_message_event` let you decide if you want to add a custom message to explain log event (`Event.Message`).

Example : For an event with his `id = 4624` (`Data.EventId`), the field `Event.Message : "An account was successfully logged on"`.

###### copy_field

The dictionary field regroups fields you want to copy under another name.

For example, is the copy field is :

```python
"copy_field": {
    "@timestamp": "datetime"
}
```

The `datetime` field will be create / update with the value of the `@timestamp` field.

###### force_type

The field `force_type` is necessary when some field in logs have possibly two different types (To correct an error, you can force one of the types - see complete error with the `-v` for the right verbosity)

```python
"force_type": {
    "Data.RawData": "str",
    "Data.RawMessage": "str"
}
```

For example, in this configuration, every field named as `Data.RawData` or `Data.RawMessage` need to be strings in database.

###### let_empty_properties

The boolean field `let_empty_properties` let you decide if you want to keep empty property of a field like :

```xml
<Field My_property="">my_value</Field>
```

(`FieldMyProperty : ""` in database)

###### let_empty_values

The boolean field `let_empty_values` let you decide if you want to keep empty field like :

```xml
<Field></Field>
```

(`Field : ""` in database - Risky)

###### let_rawdata

The boolean field `let_rawdata` let you decide if you want to keep a backup of the `Data` xml field.

###### let_rawevent

The boolean field `let_rawevent` let you decide if you want to keep a backup of the complete event. (not recommended)

###### modif_moins_to_None

The boolean field `let_rawevent` let you decide if you want to change fields which have for value a `-` such as :

```xml
<Field>-</Field>
```

The new field value will be `None` instead of `-`, if `True`.

(In Elasticsearch, a `None` value will be represented by default by a ` - ` but without any type - Can prevent types bugs)

### Using the library

Of course, if someone wants to code a script and use the library, the `evtxtoelk_launcher` is really a good example !

However, it's not really complicate :

```python
my_EvtxToElk = EvtxToElk(my_elk_config,
                         my_index_name,
                         GLOBAL_CONFIG=GLOBAL_CONFIG,
                         PARALLEL_BULK_ELK_REQUEST=500,
                         VERBOSITY=1) # Init EvtxToElk class
(nb_file_added, nb_errors) = my_EvtxToElk.evtxToElk(evtx_filename, NB_PROCESS=nb_process)
```

As we can see is this example, two things :

- First, we initialize the EvtxToElk class with the file name to treat, the elasticsearch configuration and some other optional configuration parameters.
- Then, we launch the treatment with the index name in parameter. By default, it will use the max capacity of the computer.

You can launch multiple treatment with the same class but only on the same index. You need to recreate the class if you change.

