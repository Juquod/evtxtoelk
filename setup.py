import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cm_evtxtoelk",
    version="1.0.1",
    author="Célien Menneteau",
    author_email="celien@cybex-assistance.com",
    description="Parse and send evtx logs files into Elasticsearch database by Cybex-assistance.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Juquod/evtxtoelk",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Apache 2.0 Licence",
        "Operating System :: Unix",
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    install_requires=[
        'cm_shared_generator',
        'datetime',
        'elasticsearch',
        'readchar',
        'evtx'
    ],
    scripts=[
        "scripts/evtxtoelk_launcher"
    ]
)