#!/bin/bash

sudo apt update
cwd=$(pwd)

### Python install ###
sudo apt-get install python3-pip -y
sudo pip3 install setuptools setuptools-rust

### Rust compiler install ###
sudo apt-get install rustc -y

### Install of evtx lib ###
cd /tmp
sudo apt-get install git -y
git clone https://github.com/omerbenamram/pyevtx-rs.git
cd pyevtx-rs
sudo python3 setup.py install

### Launch setup ###
cd "$cwd"
cd "$(dirname $0)/.."
pip3 install -e .