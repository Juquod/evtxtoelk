
from cm_shared_generator import Shared_Generator
from datetime import datetime
from elasticsearch import Elasticsearch, helpers
from evtx import PyEvtxParser
import json
import mmap
import multiprocessing
import os
import re
import time
import traceback
from threading import Thread
import warnings

from .evtx_messages import dict_evtx_messages
from .logger import Logger

	   # ############################# #
######### TIMEOUTPROCERROR EXCEPTION #########
	   # ############################# #

class TimeoutProcError(Exception):
	pass

	   # ################## #
######### EVTXTOELK CLASS #########
	   # ################## #

class EvtxToElk:

	_REGEX_DATE1 = re.compile(r"[A-Z][a-z]{2} [ 0-9]{1,2} [0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}")	# Regex to test if date match with the format : "mmm dd yyyy hh:mm:ss" (ex : "Nov 12 2021 20:10:00")
	_REGEX_DIVIDE_EVENT = re.compile(r"<(\w*) *(.*?)>(.*?)(?:</\1>)", re.DOTALL)	# Regex to divide subfield of a field event
	_REGEX_PARSE_PROPERTY = re.compile(r'([\w\d]*)="(.*?)"', re.DOTALL)	# Regex to get each couple key value of the parameter field
	_REGEX_SEARCH_TITLE = re.compile(r"^ *(.*?) *(?=[\n]+ *[\w ]* *:)", re.DOTALL)	# Regex which will search the title of RawData
	_REGEX_SEARCH_PROPERTIES = re.compile(r"(?:[\n]|^) *([\w ]*) *: *(.*?) *(?=(?:[\n]+ *[\w ]* *:)|(?:\n$))", re.DOTALL)	# Regex which will search all corresponding properties into RawData

	_EVTX_BYTE_STEP_FILE = 2000000	# Number of bytes to let a process in addition
	_MAXIMUM_TREATED_LOG = 2000

	_THREADSAFE_LOCK = multiprocessing.Lock()

	DEFAULT_GLOBAL_CONFIG = {
			"add_message_event": False,
			"copy_field": {},
			"force_type": {
				"Data.RawData": "str",
				"Data.RawMessage": "str",
				"System.ProviderName": "str"
			},
			"let_empty_properties": False,
			"let_empty_values": False,
			"let_rawdata": False,
			"let_rawevent": False,
			"modif_moins_to_None": True,
			"rawdata_traitment_fields": [
				"Data."
			]
		}	# Global config by default

	'''
		Initialisation of an instance of EvtxToElk :
		->	evtx_filename : The path of the evtx file to trait
		->	elk_config : The configuration of Elasticsearch class

		->	VERBOSITY : Update of the static verbosity of the class (To display more or less things) (between 0 and 1)
	'''
	def __init__(self, arg_elk_config, arg_indexname, GLOBAL_CONFIG=None, PARALLEL_BULK_ELK_REQUEST=500, VERBOSITY=1):
		super(EvtxToElk, self).__init__()
		warnings.filterwarnings('ignore', message='Elasticsearch built-in security features are not enabled')	# SUPPRESSION OF A WARNING FROM ELASTICSEARCH

		### Basic arguments ###
		## SHARED_success_evtx_to_elk -> Counter of successfully sent event to elk (In shared memory)
		## SHARED_errors_evtx_to_elk -> Counter of errors during the traitment or the sending of event (In shared memory)
		## SHARED_tries_evtx_to_elk -> Counter of send tries of log events (In shared memory)
		###

		self.SHARED_tries_evtx_to_elk = multiprocessing.Value('i', 0)
		self.SHARED_LOCK_tries_evtx_to_elk = multiprocessing.Lock()
		self.SHARED_success_evtx_to_elk = multiprocessing.Value('i', 0)
		self.SHARED_LOCK_success_evtx_to_elk = multiprocessing.Lock()
		self.SHARED_errors_evtx_to_elk = multiprocessing.Value('i', 0)
		self.SHARED_LOCK_errors_evtx_to_elk = multiprocessing.Lock()

		### Global config arguments ###
		## LOGGER -> Define to log events
		## VERBOSITY -> Define how much display the class will show
		###

		self.LOGGER = Logger(LOG_PATH=f"{os.path.dirname(__file__)}/logs", LOG_MODE=2)
		self.VERBOSITY = VERBOSITY

		### Elasticsearch arguments ###
		## INDEX_NAME -> Save of the index name
		## ELK_CONFIG -> Configuration to link with Elasticsearch database
		## ES -> Elasticsearch link
		###

		self.INDEX_NAME = arg_indexname.lower()
		self.ELK_CONFIG = arg_elk_config
		self.ES = Elasticsearch(self.ELK_CONFIG)

		### Interaction arguments ###
		## PARALLEL_BULK_ELK_REQUEST -> Number of bulk request launch in parallel (request sent to elasticsearch database)
		###

		if PARALLEL_BULK_ELK_REQUEST > 10:
			self.PARALLEL_BULK_ELK_REQUEST = PARALLEL_BULK_ELK_REQUEST
		else:
			self.PARALLEL_BULK_ELK_REQUEST = 500
		_MAXIMUM_TREATED_LOG = self.PARALLEL_BULK_ELK_REQUEST * 4

		### Global config arguments ###
		## ADD_MESSAGE_EVENT (bool) -> If True, add a custom message to explain log event (Event.Message)
		## FORCE_TYPE (dict) -> Define fields on which we need to force typing
		## LET_EMPTY_PROPERTIES (bool) -> Allow to keep all empty properties (False to delete then)
		## LET_EMPTY_VALUES (bool) -> Allow to keep all empty field (False to delete then)
		## LET_RAWDATA (bool) -> Allow to keep raw datas in addition to parse properties (Event size can be grow fast in True)
		## LET_RAWEVENT (bool) -> Allow to keep the entire raw event in addition to parse properties (Event size can be grow fast in True)
		## MODIF_MOINS_TO_NONE (bool) -> If True, replace all value '-' by None
		## RAWDATA_TRAITMENT_FIELDS -> Field define to be traited as RawData
		###

		if GLOBAL_CONFIG is None:
			self.ADD_MESSAGE_EVENT = EvtxToElk.DEFAULT_GLOBAL_CONFIG["add_message_event"]
			self.COPY_FIELD = EvtxToElk.DEFAULT_GLOBAL_CONFIG["copy_field"]
			self.FORCE_TYPE = EvtxToElk.DEFAULT_GLOBAL_CONFIG["force_type"]
			self.LET_EMPTY_PROPERTIES = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_empty_properties"]
			self.LET_EMPTY_VALUES = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_empty_values"]
			self.LET_RAWDATA = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_rawdata"]
			self.LET_RAWEVENT = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_rawevent"]
			self.MODIF_MOINS_TO_NONE = EvtxToElk.DEFAULT_GLOBAL_CONFIG["modif_moins_to_None"]
			self.RAWDATA_TRAITMENT_FIELDS = EvtxToElk.DEFAULT_GLOBAL_CONFIG["rawdata_traitment_fields"]
		else:

			# ADD_MESSAGE_EVENT
			if "add_message_event" in GLOBAL_CONFIG:
				self.ADD_MESSAGE_EVENT = GLOBAL_CONFIG["add_message_event"]
				if not isinstance(self.ADD_MESSAGE_EVENT, bool):
					self.LOGGER.error('[-] ADD_MESSAGE_EVENT must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.ADD_MESSAGE_EVENT = EvtxToElk.DEFAULT_GLOBAL_CONFIG["add_message_event"]

			# COPY_FIELD
			self.COPY_FIELD = EvtxToElk.DEFAULT_GLOBAL_CONFIG["copy_field"]
			if "copy_field" in GLOBAL_CONFIG:
				if not isinstance(GLOBAL_CONFIG["copy_field"], dict):
					self.LOGGER.error('[-] Field "copy_field" in GLOBAL_CONFIG must be a dictionary* !\n', VERBOSITY=self.VERBOSITY)
				else:
					self.COPY_FIELD = GLOBAL_CONFIG["copy_field"]

			# FORCE_TYPE
			self.FORCE_TYPE = EvtxToElk.DEFAULT_GLOBAL_CONFIG["force_type"]
			if "force_type" in GLOBAL_CONFIG:
				if not isinstance(GLOBAL_CONFIG["force_type"], dict):
					self.LOGGER.error('[-] Field "force_type" in GLOBAL_CONFIG must be a dictionary !\n', VERBOSITY=self.VERBOSITY)
				else:
					self.FORCE_TYPE.update(GLOBAL_CONFIG["force_type"])

			# LET_EMPTY_PROPERTIES
			if "let_empty_properties" in GLOBAL_CONFIG:
				self.LET_EMPTY_PROPERTIES = GLOBAL_CONFIG["let_empty_properties"]
				if not isinstance(self.LET_EMPTY_PROPERTIES, bool):
					self.LOGGER.error('[-] LET_EMPTY_PROPERTIES must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.LET_EMPTY_PROPERTIES = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_empty_properties"]

			# LET_EMPTY_VALUES
			if "let_empty_values" in GLOBAL_CONFIG:
				self.LET_EMPTY_VALUES = GLOBAL_CONFIG["let_empty_values"]
				if not isinstance(self.LET_EMPTY_VALUES, bool):
					self.LOGGER.error('[-] LET_EMPTY_VALUES must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.LET_EMPTY_VALUES = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_empty_values"]

			# LET_RAWDATA
			if "let_rawdata" in GLOBAL_CONFIG:
				self.LET_RAWDATA = GLOBAL_CONFIG["let_rawdata"]
				if not isinstance(self.LET_RAWDATA, bool):
					self.LOGGER.error('[-] LET_RAWDATA must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.LET_RAWDATA = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_rawdata"]

			# LET_RAWEVENT
			if "let_rawevent" in GLOBAL_CONFIG:
				self.LET_RAWEVENT = GLOBAL_CONFIG["let_rawevent"]
				if not isinstance(self.LET_RAWEVENT, bool):
					self.LOGGER.error('[-] LET_RAWEVENT must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.LET_RAWEVENT = EvtxToElk.DEFAULT_GLOBAL_CONFIG["let_rawevent"]

			# MODIF_MOINS_TO_NONE
			if "modif_moins_to_None" in GLOBAL_CONFIG:
				self.MODIF_MOINS_TO_NONE = GLOBAL_CONFIG["modif_moins_to_None"]
				if not isinstance(self.MODIF_MOINS_TO_NONE, bool):
					self.LOGGER.error('[-] MODIF_MOINS_TO_NONE must be a bool !\n', VERBOSITY=self.VERBOSITY)
					raise
			else:
				self.MODIF_MOINS_TO_NONE = EvtxToElk.DEFAULT_GLOBAL_CONFIG["modif_moins_to_None"]

			# RAWDATA_TRAITMENT_FIELDS
			self.RAWDATA_TRAITMENT_FIELDS = EvtxToElk.DEFAULT_GLOBAL_CONFIG["rawdata_traitment_fields"]
			if "rawdata_traitment_fields" in GLOBAL_CONFIG:
				if not isinstance(GLOBAL_CONFIG["rawdata_traitment_fields"], list):
					self.LOGGER.error('[-] Field "rawdata_traitment_fields" in GLOBAL_CONFIG must be a list !\n', VERBOSITY=self.VERBOSITY)
				else:
					self.RAWDATA_TRAITMENT_FIELDS = GLOBAL_CONFIG["rawdata_traitment_fields"]


	   # ######################### #
######### GETTER AND SETTER PART #########
	   # ######################### #

	'''
		Getter which count the number of process and give them a unique id
	'''
	_NUMBER_OF_PROCESS_ = multiprocessing.Value('i', 0)
	_NUMBER_OF_PROCESS_LOCK_ = multiprocessing.Lock()
	def _get_NUMBER_OF_PROCESS_(self):
		process = -1
		self._NUMBER_OF_PROCESS_LOCK_.acquire()
		try:
			self._NUMBER_OF_PROCESS_.value += 1
			process = self._NUMBER_OF_PROCESS_.value
		finally:
			self._NUMBER_OF_PROCESS_LOCK_.release()
		return process


	def get_log_filename(self):
		return self.LOGGER.get_log_path()

	'''
		Functions which allow to get the total of success, errors and tries of the global EvtxToElk class instance
	'''
	def get_total_success(self): return self.SHARED_success_evtx_to_elk.value
	def get_total_errors(self): return self.SHARED_errors_evtx_to_elk.value
	def get_total_tries(self): return self.SHARED_tries_evtx_to_elk.value


	'''
		Update the max field number into the elk database (Can be used if the index doesn't exist yet in database !)
	'''
	def update_field_number(self, number):
		self.ES.indices.put_settings({ "index.mapping.total_fields.limit" : number }, self.INDEX_NAME)

	   # ################### #
######### LOG PARSING PART #########
	   # ################### #

	'''
		Parse the log event to be sent :
		input -->	<Event xmlns="http://schemas.microsoft.com/win/2004/08/events/event">
						<System>
							<Provider Name="DPTF"></Provider>
							<EventID Qualifiers="0">256</EventID>
							<Version>0</Version>
							<Level>2</Level>
							<Task>1</Task>
							...
						</System>
						<EventData>
							<Data>&lt;string&gt;Intel(R) Dynamic Platform and Thermal Framework&lt;/string&gt;
								&lt;string&gt;ESIF(8.3.10207.5567) TYPE: ERROR MODULE: DPTF TIME 38145125 ms

								DPTF Build Version:  8.3.10207.5567
								DPTF Build Date:  Nov  2 2017 14:28:00
								...
							</Data>
							<Binary></Binary>
						</EventData>
					</Event>
		into something like that :
		output <--	{
						'Event.xmlns': 'http://schemas.microsoft.com/win/2004/08/events/event',
						'System.Provider.Name': 'DPTF',
						'System.EventID.Qualifiers': '0',
						'System.EventID': '256',
						'System.Version': '0',
						'System.Level': '2',
						'System.Task': '1',
						...
						'Data.Title': 'Intel(R) Dynamic Platform and Thermal Framework\nESIF(8.3.10207.5567) TYPE: ERROR MODULE: DPTF TIME 38145125 ms',
						'Data.DPTFBuildVersion': '8.3.10207.5567',
						'Data.DPTFBuildDate': 'Nov  2 2017 14:28:00'
					}
	'''
	def _parse_LogEvent_(self, arg_event):

		if not isinstance(arg_event, str):
			return { 'Data.RawData': arg_event }

		datas = {}
		if self.LET_RAWEVENT:
			datas['Event.RawEvent'] = arg_event

		### Function which parse some field properties ###
		def parse_properties(arg_properties):
			result = EvtxToElk._REGEX_PARSE_PROPERTY.findall(arg_properties)

			datas = {}
			for key, value in result:
				if self.LET_EMPTY_PROPERTIES or value != '':
					datas[key] = value

			return datas

		### Function which will parse a data field ###
		def field_parse(arg_datas, global_name=''):
			result = EvtxToElk._REGEX_DIVIDE_EVENT.findall(arg_datas)

			if result:

				datas = {}
				for key, params, value in result:
					if key == 'Data':
						key = ''
						tmp_global_key = global_name 	# {global_name}{key}
						datas_parsed = field_parse(value, global_name=tmp_global_key)		# Parse value of field as a sub-field if possible
					else:
						key = key.replace(' ', '')
						tmp_global_key = global_name + key 	# {global_name}{key}
						datas_parsed = field_parse(value, global_name=tmp_global_key+'.')		# Parse value of field as a sub-field if possible

					properties = parse_properties(params)	# Parse properties of the field

					if isinstance(datas_parsed, str):

						if len(properties) == 0:

							if re.match(r"^[ \t\n]*$", value) is None:	# <Field>30</Field>  -->  Global_Name.Field : '30'
								if len(key) == 0:	# tmp_global_key = Data.	-->  Data.RawData
									tmp_key = tmp_global_key+"RawData"
									if tmp_key in datas: datas[tmp_key] += '\n'+value
									else: datas[tmp_key] = value
								else:
									datas[tmp_global_key] = value
							elif self.LET_EMPTY_VALUES:	# <Field></Field>  -->  Global_Name.Field : ''
								datas[tmp_global_key] = None

						else : # len_properties > 0

							if "Name" in properties:
								name = properties.pop("Name")

								if re.match(r"^[ \t\n]*$", value) is None:	# <Field Name="Something1">30</Field>  -->  Global_Name.FieldSomething1 : '30'
									datas[f'{tmp_global_key}{name}'] = value
								elif self.LET_EMPTY_VALUES:
									datas[f'{tmp_global_key}{name}'] = None
								elif tmp_global_key != 'Data.':	# <Field Name="Something1"></Data>  -->  Global_Name.FieldName : ''
									datas[tmp_global_key+'Name'] = name

							else :	# <Field Prop1="something1" Prop2="something2">30</Field> --> Global_Name.Field : 30 ...
								if re.match(r"^[ \t\n]*$", value) is None:
									datas[tmp_global_key] = value
								elif self.LET_EMPTY_VALUES:
									datas[tmp_global_key] = None

							for prop in properties:	# <Field Prop1="something1" Prop2="something2">30</Field> --> ... Global_Name.FieldProp1 : something1, Global_Name.FieldProp2 : something2
								datas[f'{tmp_global_key}{prop}'] = properties[prop]


					elif isinstance(datas_parsed, dict):
						datas.update(datas_parsed)
					else:
						raise

				return datas
			else:
				return arg_datas
					

		event = EvtxToElk._REGEX_DIVIDE_EVENT.search(arg_event)	# First split of the event to get subfields and then to parse it

		### Add properties of the global event ###
		properties = parse_properties(event.group(2))
		for key in properties:	# Add event properties
			datas[f'Event.{key}'] = properties[key]

		### Parsing of the events datas ###
		result = EvtxToElk._REGEX_DIVIDE_EVENT.findall(event.group(3))	# Search in the data event
		for key, params, value in result:
			if params != '':
				datas.update(parse_properties(params))

			if key == 'EventData':
				if self.LET_RAWDATA:
					datas['Data.RawData'] = value
				fields = field_parse(value, global_name='Data.')
			else:
				fields = field_parse(value, global_name=f'{key}.')

			if isinstance(fields, dict):
				datas.update(fields)
			else:
				#print("---------------------------\n",fields,"\n---------------------------")
				pass

		for field in self.RAWDATA_TRAITMENT_FIELDS:
			if field in datas:
				if "System.Channel" in datas:
					datas.update(self._parse_rawData_(datas.pop(field), CHANNEL=datas["System.Channel"]))
				else:
					datas.update(self._parse_rawData_(datas.pop(field)))
		### ###

		return datas


	'''
		Try to give some type to values (You can force typing with FORCE_TYPE dict)
			input --> 	{
							"key1" : "Blabla"
							"key2" : "012984782"
						}
		And then, it will return :
			output <--	{
							"key1" : "Blabla"
							"key2" : 012984782
						}
	'''
	def _give_types_to_values_(self, arg_datas):
		force_type = self.FORCE_TYPE
		datas_treated = {}

		keys_force = list(force_type.keys())
		for key in arg_datas:
			value = arg_datas[key]

			if value is None or len(value) < 1 or ( self.MODIF_MOINS_TO_NONE and value[0] == '-' ) :
				datas_treated[key]=None
				continue

			### Force typing ###

			b_force = False
			for key_force in keys_force:
				if key_force in key:
					b_force = True
					typef = force_type[key_force]
					if typef == "str":
						datas_treated[key] = str(value)
					elif typef == "int":
						try:
							value=int(value)
							if value > 9223372036854775807 or value < -9223372036854775808 :
								self.LOGGER.error(f'[-] Impossible to parse {arg_datas[key]} into Integer type (OUT OF RANGE) !\n', VERBOSITY=self.VERBOSITY-1)
								datas_treated[key]=None
							else :
								datas_treated[key]=value
						except:
							self.LOGGER.error(f'[-] Impossible to parse {arg_datas[key]} into Integer type !\n', VERBOSITY=self.VERBOSITY-1)
							datas_treated[key]=None
							pass
					else:
						self.LOGGER.error(f'[-] Impossible to parse {arg_datas[key]} into {typef} type !\n', VERBOSITY=self.VERBOSITY-1)
						datas_treated[key]=None
					continue
			if b_force:
				continue

			### ############ ###

			try:
				value = int(value)
				if value > 9223372036854775807 or value < -9223372036854775808 :
					value = arg_datas[key]
					raise
				datas_treated[key+'#'] = value
				continue
			except:
				keyu = key.upper()
				if 'DATE' in keyu or 'TIME' in keyu:
					if value != '':
						date = self._format_datetime_str_(value)
						if date is not None:
							datas_treated[key] = date
							continue

			datas_treated[key] = arg_datas[key]
		
		return datas_treated


	'''
		Return a string from a datetime or a another string formated to a common format
	'''
	def _format_datetime_str_(self, arg_date):
		if isinstance(arg_date, str):
			try:
				arg_date = arg_date.replace(" UTC", '')
				if 'T' in arg_date:
					date = datetime.strptime(arg_date, "%Y-%m-%dT%H:%M:%S.%fZ")
				elif ':' in arg_date:
					if '.' in arg_date:
						date = datetime.strptime(arg_date, "%Y-%m-%d %H:%M:%S.%f")
					else:
						date = datetime.strptime(arg_date, "%Y-%m-%d %H:%M:%S")
				else:
					date = datetime.strptime(arg_date, "%Y-%m-%d")
			except:
				if EvtxToElk._REGEX_DATE1.match(arg_date):
					date = datetime.strptime(arg_date, "%b %d %Y %H:%M:%S")
				else:
					self.LOGGER.warning(f'[!] Func format_datetime_str : Bad date format "{arg_date}" ! (1)\n', VERBOSITY=self.VERBOSITY)
					return None
		elif isinstance(arg_date, datetime):
			date = arg_date
		else:
			self.LOGGER.warning(f'[!] Func format_datetime_str : Bad date format "{arg_date}" ! (2)\n', VERBOSITY=self.VERBOSITY)
			return None
		return date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


	   # ######################### #
######### RAW DATAS PARSING PART #########
	   # ######################### #

	'''
		List of modules to apply according to the Channel of the log file
		For example, the module _module_application_() will be apply of the Application logs file
	'''

	'''
		Function which will parse RawData of files according to their channel
	'''
	def _parse_rawData_(self, raw_data, CHANNEL=None):
		
		if CHANNEL is not None:
			if CHANNEL == "Application":
				return self._module_application_(raw_data)
		else:
			pass

		return {}


	'''
		Parsing of RawData of Application logs file parts :
			input -->	<string>Intel(R) Dynamic Platform and Thermal Framework</string>
						<string>ESIF(8.3.10207.5567) TYPE: ERROR MODULE: DPTF TIME 38144215 ms

						DPTF Build Version:  8.3.10207.5567
						DPTF Build Date:  Nov  2 2017 14:28:00
						</string>
		to get something like :
			output <--	{	'RawData': '	<string>Intel(R) Dynamic Platform and Thermal Framework</string>
											<string>ESIF(8.3.10207.5567) TYPE: ERROR MODULE: DPTF TIME 38144215 ms

											DPTF Build Version:  8.3.10207.5567
											DPTF Build Date:  Nov  2 2017 14:28:00
											</string>',
							'Data.DPTFBuildVersion': '8.3.10207.5567',
							'Data.DPTFBuildDate': 'Nov  2 2017 14:28:00'
						}
	'''
	def _module_application_(self, raw_data):
		if not isinstance(raw_data, str):
			return { 'Data.RawData': raw_data }

		raw_data = raw_data.replace('&lt;string&gt;', '').replace('&lt;/string&gt;', '')
		datas = {}

		total_properties = 0
		title = EvtxToElk._REGEX_SEARCH_TITLE.search(raw_data)
		if title is not None:
			datas['Data.Title'] = title[0]
			total_properties = 1

		result = EvtxToElk._REGEX_SEARCH_PROPERTIES.findall(raw_data)

		for key, value in result:
			key = key.replace(' ', '')
			if value !='' or self.LET_EMPTY_VALUES:
				datak = f'Data.{key}'

				i = 1
				add = ''
				while datak+add in datas:
					i+=1
					add = str(i)

				datas[datak+add] = value

			total_properties += 1

		if total_properties == 0 :
			datas['Data.RawMessage'] = raw_data

		return datas


	   # ################## #
######### PROCESSING PART #########
	   # ################## #

	'''
		This function will send every events in bulk_queue to elasticsearch.
		It will add into SHARED_SUCCESS the number of events successfully sent and into SHARED_ERRORS the number of errors
		PARAMS :
			arg_bulk_queue -> The list of the requests to send
			SHARED_SUCCESS -> Shared variable of the number of success (will be update according to the success of the function)
			SHARED_ERRORS -> Shared variable of the number of errors (will be update according to the errors of the function)
	'''
	def _bulk_to_elasticsearch_(self, arg_bulk_queue, SHARED_SUCCESS=None, SHARED_ERRORS=None, RETRY_IF_FULL_ERROR=False):
		count = 0
		nb_success = 0
		len_bulk_queue = len(arg_bulk_queue)

		try:
			bulk_generator = helpers.parallel_bulk(self.ES, arg_bulk_queue, raise_on_error=False, raise_on_exception=False, request_timeout=200)
			with self.SHARED_LOCK_tries_evtx_to_elk: self.SHARED_tries_evtx_to_elk.value += len_bulk_queue
			while count < len_bulk_queue:
				success, info = next(bulk_generator)
				count+=1
				if not success:

					try:
						reason = info['index']['error']['reason']
						if "Limit of total fields" in reason:
							result = re.search(r"\d+", reason)
							self.update_field_number(int(result.group(0))+1000)
						elif "ConnectionTimeout" in reason:
							self.LOGGER.error('[-] Fail to send an event to Elk : CONNECTION TIMEOUT\n', VERBOSITY=self.VERBOSITY)
							continue
					except:
						pass

					if info is not None:
						info = str(info)+'\n'
					self.LOGGER.error('[-] Fail to send an event to Elk !\n', INFO=info, VERBOSITY=self.VERBOSITY)
				else:
					nb_success += 1

		except:
			if nb_success != 0 or not RETRY_IF_FULL_ERROR:
				trace = traceback.format_exc()
				if trace is not None:
					trace = str(trace)+'\n'
			else:
				trace = None
			self.LOGGER.error(f'[-] Fail to send ({len_bulk_queue - nb_success}) events to Elk !\n', INFO=trace, VERBOSITY=self.VERBOSITY)

		nb_error = len_bulk_queue - nb_success
		if nb_success == 0 and len_bulk_queue != 0 and RETRY_IF_FULL_ERROR:
			time.sleep(0.2)
			self.LOGGER.error(f'[ ] Retry to send failed events ({nb_error})...\n', VERBOSITY=self.VERBOSITY)
			self._bulk_to_elasticsearch_(json.loads(json.JSONEncoder().encode(arg_bulk_queue), strict=False), SHARED_SUCCESS, SHARED_ERRORS, RETRY_IF_FULL_ERROR=False)
		else:
			with self.SHARED_LOCK_errors_evtx_to_elk: self.SHARED_errors_evtx_to_elk.value += nb_error
			with self.SHARED_LOCK_success_evtx_to_elk: self.SHARED_success_evtx_to_elk.value += nb_success
			if SHARED_ERRORS is not None: SHARED_ERRORS.value += nb_error
			if SHARED_SUCCESS is not None: SHARED_SUCCESS.value += nb_success


	'''
		Function launch as a process which will treat and send event from evtx log file to elasticsearch
		This function is writing to be launch in a independant process.
		PARAMS :
			arg_process_number -> The id number of the process define previously by _get_NUMBER_OF_PROCESS_
			arg_parallel_generator -> The parallel_generator which give logs to treat
			shared_success -> Shared variable of the number of success
			shared_errors -> Shared variable of the number of errors
			VERBOSITY -> Number of verbosity of the process (DEFAULT = 0)
	'''
	def _evtxToElk_process_(self, arg_process_number, arg_parallel_generator, arg_filename, shared_success, shared_errors, VERBOSITY=0):
		if self.VERBOSITY > 1 : print(f"[+] Process number {arg_process_number} launch !")

		bulk_queue = []	# List of event to send to Elasticsearch
		sending_thread = None	# The process which send events to Elasticsearch

		while True:
			try: record = arg_parallel_generator.next()
			except StopIteration: break

			if record is None: break

			if 'data' in record: xml = record['data']
			else: continue

			### Traitment of the event ###

			event_data_to_elk = self._parse_LogEvent_(xml)	# Parse the log event
			event_data_to_elk = self._give_types_to_values_(event_data_to_elk)	# Give types to values

			### Adding of some useful fields ###

			if 'System.TimeCreatedSystemTime' in event_data_to_elk:
				event_data_to_elk['@timestamp'] = event_data_to_elk['System.TimeCreatedSystemTime']	# Copy the datetime field
			event_data_to_elk["_index"] = self.INDEX_NAME	# Adding of the index name
			event_data_to_elk["parser"] = "winevtx"
			event_data_to_elk["Event.FromFileName"] = arg_filename

			### Adding of a personalized message ###

			if self.ADD_MESSAGE_EVENT:

				msg = "["
				if "System.EventID#" in event_data_to_elk: msg += str(event_data_to_elk["System.EventID#"])
				msg += "/"

				try: msg += f"{dict_evtx_messages[event_data_to_elk['System.ProviderName']][event_data_to_elk['System.EventID#']]}"
				except: msg += '?'
				msg+="]"

				try: msg += f" Provider : \"{event_data_to_elk['System.ProviderName']}\""
				except: pass
				try: msg += f", Computer : \"{event_data_to_elk['System.Computer']}\""
				except: pass
				#try: msg += f", Record Number : {event_data_to_elk['System.EventRecordID']}"
				#except: pass
				#try: msg += f", Event Level : {event_data_to_elk['System.Level']}"
				#except: pass

				msg += ", Datas : ["
				i=0
				for key in event_data_to_elk:
					if "Data." in key:
						if i > 0:
							msg += ", "
						msg += str(event_data_to_elk[key])
						i+=1
				msg += "]"

				event_data_to_elk["Event.Message"] = msg


			### Copy of fields ###

			for key, value in self.COPY_FIELD.items():
				try: event_data_to_elk[value] = event_data_to_elk[key]
				except: pass

			### Sending of datas into Elk ###
			bulk_queue.append(event_data_to_elk)	# Adding the event to the queue to be send

			len_bulk_queue = len(bulk_queue)
			if len_bulk_queue >= self.PARALLEL_BULK_ELK_REQUEST:

				if sending_thread is not None and sending_thread.is_alive():
					if len_bulk_queue < EvtxToElk._MAXIMUM_TREATED_LOG:
						continue
					else:
						sending_thread.join(timeout=160)
						if sending_thread.is_alive():
							self.LOGGER.error(f'[-] Timeout of sending thread !\n', VERBOSITY=self.VERBOSITY)

				if VERBOSITY > 0 and self.VERBOSITY > 0 : print(f'[ ] (P{arg_process_number}) Evtx logs sent successfully into Elasticsearch : {shared_success.value} - {shared_errors.value} error(s)')
				sending_thread = Thread(target=self._bulk_to_elasticsearch_, args=(bulk_queue, shared_success, shared_errors, True))
				sending_thread.start()

				bulk_queue = []

			### ######################### ###

		### Sending of rest of the datas into Elk ###

		if len(bulk_queue) > 0:
			self._bulk_to_elasticsearch_(bulk_queue, shared_success, shared_errors, True)
			bulk_queue = []

		if sending_thread is not None and sending_thread.is_alive():
			sending_thread.join(timeout=160)
			if sending_thread.is_alive():
				self.LOGGER.error(f'[-] Timeout of sending thread !\n', VERBOSITY=self.VERBOSITY)

		if self.VERBOSITY > 0:
			if VERBOSITY > 0 : print(f'[ ] (P{arg_process_number}) Evtx logs sent successfully into Elasticsearch : {shared_success.value} - {shared_errors.value} error(s)')
			if self.VERBOSITY > 1 : print(f"[+] Process number {arg_process_number} finished !")

		### ######################### ###


	'''
		Function which will send event from evtx log file to elasticsearch
		It will launch a multiple of process.
		PARAMS :
			arg_evtx_filename -> The name of the evtx file to treat
			NB_PROCESS -> The number of process MAXIMUM to launch (DEFAULT : The best number of process needed using the full processor)
	'''
	def evtxToElk(self, arg_evtx_filename, NB_PROCESS=None):
		
		try: evtx_xmlgenerator = PyEvtxParser(arg_evtx_filename).records()
		except: raise "Error in PyEvtxParser !"

		## Count the number of process needed ##

		if NB_PROCESS is None:
			NB_PROCESS = len(os.sched_getaffinity(0))

		stat_file = os.stat(arg_evtx_filename)
		nb_max_process = int( stat_file.st_size / EvtxToElk._EVTX_BYTE_STEP_FILE ) + 1
		if NB_PROCESS > nb_max_process: NB_PROCESS = nb_max_process
		if NB_PROCESS <= 0: NB_PROCESS = 1

		if self.VERBOSITY > 2 : print(f'[ ] Number of process decided : {NB_PROCESS}')

		EvtxToElk._THREADSAFE_LOCK.acquire()
		try:
			## Launch the shared generator ##

			shared_success = multiprocessing.Value('i', 0)
			shared_errors = multiprocessing.Value('i', 0)
			shared_generator = Shared_Generator(evtx_xmlgenerator, FORCE=True, NUMBER_PUT_RETRY=19, TIMEOUT=3)	# A generator shared with multiple process to read file and share datas. (It's use to read log by log the evtx file)
			shared_generator.start(NEW_PROCESS=False)

			### Launching process ###

			l_process = []	# List which will contain the list of the launched process
			proc = multiprocessing.get_context().Process(target=self._evtxToElk_process_, args=(self._get_NUMBER_OF_PROCESS_(), shared_generator, arg_evtx_filename, shared_success, shared_errors, self.VERBOSITY))
			proc.start()
			l_process.append(proc)

			i = 1
			while i < NB_PROCESS:
				proc = multiprocessing.get_context().Process(target=self._evtxToElk_process_, args=(self._get_NUMBER_OF_PROCESS_(), shared_generator, arg_evtx_filename, shared_success, shared_errors))
				proc.start()
				l_process.append(proc)
				i+=1

		finally: EvtxToElk._THREADSAFE_LOCK.release()

		### Waiting the deaths process ###

		for proc in l_process:
			tmp = shared_success.value + shared_errors.value
			proc.join(timeout=60)
			while proc.is_alive():
				if tmp == shared_success.value + shared_errors.value:
					for proc2 in l_process:
						if proc2.is_alive():
							proc2.kill()
					shared_generator.stop()
					self.LOGGER.error(f'[-] Timeout : Process killed ! (file = {arg_evtx_filename})\n', VERBOSITY=self.VERBOSITY)
					raise TimeoutProcError
				tmp = shared_success.value + shared_errors.value
				proc.join(timeout=60)

		shared_generator.stop()

		self.LOGGER.success(f"[+] Sending finished successfully (file : {arg_evtx_filename} - index : {self.INDEX_NAME}) :\n    | SUCCESS : {shared_success.value}\n    | ERRORS : {shared_errors.value}\n", VERBOSITY=self.VERBOSITY)
			
		return shared_success.value, shared_errors.value


	   # ###################### #
######### END EVTXTOELK CLASS #########
	   # ###################### #