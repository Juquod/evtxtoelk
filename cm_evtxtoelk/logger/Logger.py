
from datetime import datetime
import os
import sys

class Logger():

	def __init__(self, LOG_PATH="logs/", LOG_MODE=1):

		if LOG_PATH[len(LOG_PATH)-1] != "/":
			LOG_PATH += '/'
		if not os.path.exists(LOG_PATH) or not os.path.isdir(LOG_PATH):
			os.mkdir(LOG_PATH)

		self._filename = LOG_PATH + datetime.now().strftime("%Y-%m-%d_%H") + '.log'
		self.LOG_MODE = LOG_MODE

	def get_log_path(self):
		return self._filename

	def _add_logs_(self, data):
		with open(self._filename, "a") as log_file:
			log_file.write(datetime.now().strftime("%H:%M:%S -- ") + data)

	def error(self, error, INFO=None, VERBOSITY=1):
		try:
			if VERBOSITY > 0:
				sys.stderr.write(error)
				if VERBOSITY > 1:
					sys.stderr.write(info)
			if INFO is not None:
				error += INFO
			self._add_logs_(error)
		except:
			pass

	def warning(self, warn, INFO=None, VERBOSITY=1):
		try:
			if VERBOSITY > 1:
				sys.stderr.write(warn)
				if VERBOSITY > 2:
					sys.stderr.write(info)
			if self.LOG_MODE > 1 :
				if INFO is not None:
					warn += INFO
				self._add_logs_(warn)
		except:
			pass

	def success(self, success, VERBOSITY=1):
		try:
			if VERBOSITY > 0:
				sys.stderr.write(success)
			if self.LOG_MODE > 0 :
				self._add_logs_(success)
		except:
			pass
